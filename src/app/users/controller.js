const extractObject = require( "../../utilities/" ).extractObject;
const logger = require( "../../utilities/logger" );
const repository = require( "./repository" );

exports.register = ( req, res, next ) => {
    const { user } = req;

    if ( user ) {
        logger.error( "User already exists" );
        res.preconditionFailed( "existing_user" );
        return;
    }
    repository.saveUser( req.body, (err, user) => {
        if (err) {
            return next(err);
        }
        res.success( extractObject(
            user,
            [ "_id", "username" ],
        ) );
    });

};

exports.edit = async ( req, res ) => {
    try {
        const user = await repository.findUser( req.user.id );
        const editedUser = await repository.editUser( user, req.body );
        res.success( extractObject(
            editedUser,
            [ "id", "username" ],
        ) );
    } catch ( err ) {
        res.send( err );
    }
};

exports.delete = async ( req, res ) => {
    try {
        const user = await repository.findUser( req.user.id );
        const deletedUser = await repository.deleteUser( user, req.body );
        console.log( deletedUser );
        res.success( extractObject(
            deletedUser,
            [ "id", "username" ],
        ) );
    } catch ( err ) {
        console.log( err );
        res.send( err );
    }
};
