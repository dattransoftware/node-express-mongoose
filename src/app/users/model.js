const moment = require('moment');
const mongoose = require( "mongoose" );
const config = require('../../config/index');
const bcrypt = require('bcrypt-nodejs');

const UserSchema = mongoose.Schema( {
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true,
        enum: [ "admin", "guess" ]
    },
    created_time: {
		type: Number
	},
	updated_time: Number,
});

UserSchema.pre('save', function(next) {
    this.increment();
	let now = +moment.utc();

	if( this.isNew ) {
		this.created_time = now;
	}
    this.updated_time = now;
    if (this.password && this.isModified('password')) {
		this.password = this.generateHash(this.password);
	}

    next();
});

UserSchema.methods.generateHash = function( password ) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(16), null);
};

UserSchema.methods.authenticate = function( password ) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model( "User", UserSchema, config.dbTablePrefix.concat("users") );
