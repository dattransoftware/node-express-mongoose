const mongoose = require( "mongoose" );

const User = mongoose.model( "User" );

const saveUser = ( data, next ) => {
    const user = new User( data );

    user.save((err) => {
        if (err) {
            return next(err);
        }
        if (next) {
            return next(null, user);
        }
    });
};

const editUser = ( user, data ) => {
    const { name, sex, age } = data;
    const currentUser = user;

    currentUser.name = name;
    currentUser.sex = sex;
    currentUser.age = age;
    return user.save( );
};

const deleteUser = ( user ) => user.remove();

const findUser = ( id ) => User.findOne( { id } );

module.exports = {
    saveUser,
    editUser,
    deleteUser,
    findUser,
};
