module.exports = {
    host: "127.0.0.1",
    port: 3000, // change with development port
    mongoUrl: "mongodb://localhost:27017/report_dev", // replace "projectDbName" with a proper db name
    logLevel: "debug", // can be chenged to error, warning, info, verbose or silly
    secret: "$2y$12$KYh./AOiSxKIGe4ujQH3vezfQIOTjXHQGcIYzCTqgXEgtWor05NWa",
    dbTablePrefix: "rp_dev_",
    basePath: '/api/v1',
};
