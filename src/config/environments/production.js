module.exports = {
    host: "127.0.0.1",
    port: 3000, // change with production port
    mongoUrl: process.env.CONNECTION_STRING,
    logLevel: process.env.LOG_LEVEL,
    secret: process.env.SECRET,
    dbTablePrefix: "rp_prod_",
    basePath: "/api/v1",
};
